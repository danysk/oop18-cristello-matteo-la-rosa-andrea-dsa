package view.board;

/**
 * This is the interface for a 2D point.
 */
public interface Point2D {

    /**
     * @return The X coordinate value.
     */
    int getX();

    /**
     * @return The Y coordinate value.
     */
    int getY();

}
