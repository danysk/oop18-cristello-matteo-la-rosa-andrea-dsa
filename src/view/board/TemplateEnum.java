package view.board;

/**
 * This enumeration is used to create the choosent type of board.
 *
 */
public enum TemplateEnum {

    /**
     * This enum is used to discriminate the a ZigZag shape for the tileline.
     */
    ZIGZAG,
    /**
     * This enum is used to discriminate the a Circular shape for the tileline.
     */
    CIRCULAR;

}
