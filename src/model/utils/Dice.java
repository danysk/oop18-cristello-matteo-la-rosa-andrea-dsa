package model.utils;

/**
 * This class is an interface for a generic dice dices.
 */

public interface Dice {

    /**
     * @return an int value
     */
    int getValue();


}
