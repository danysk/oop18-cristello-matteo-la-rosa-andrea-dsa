package model.utils;

/**
 * This enumerator is a list of possible direction of a player.
 * 
 */

public enum Direction {

    /**
     * This is direction deep enum.
     */
    DEEP,
    /**
     * This is direction to_boat enum.
     */
    TO_BOAT,
    /**
     * This is direction nowhere enum.
     */
    NOWHERE;
}
